let lainnya = "other links";
let space = "works projects sites";

const initialDetails = [
  {
    title: "Space",
    link: "",
    category: "Kongkow Space, Good Morning, Pakde.AI, WC Umum" + space,
    new: false,
    divider: true,
  },
  {
    title: "Kongkow Space",
    link: "https://kongkow.space",
    category: space,
    new: true,
    divider: false,
  },
  {
    title: "Good Morning",
    link: "https://morning.kongkow.space",
    category: space,
    new: false,
    divider: false,
  },
  {
    title: "Pakde.AI",
    link: "https://pakde.moojok.xyz",
    category: space,
    new: false,
    divider: false,
  },
  {
    title: "WC Umum",
    link: "https://wcumum.kongkow.space",
    category: space,
    new: false,
    divider: false,
  },
  {
    title: "Lainnya",
    link: "",
    category:
      "Instagram, Github, Gitlab, Codepen, Unstoppable Domains, Analytics, Source Code" +
      lainnya,
    new: false,
    divider: true,
  },
  {
    title: "Instagram",
    link: "https://otw.kongkowitpku.xyz/instagram",
    category: lainnya,
    new: false,
    divider: false,
  },
  {
    title: "Github",
    link: "https://otw.kongkowitpku.xyz/github",
    category: lainnya,
    new: false,
    divider: false,
  },
  {
    title: "Gitlab",
    link: "https://otw.kongkowitpku.xyz/gitlab",
    category: lainnya,
    new: false,
    divider: false,
  },
  {
    title: "Unstoppable Domains",
    link: "https://otw.kongkowitpku.xyz/uns",
    category: lainnya,
    new: false,
    divider: false,
  },
  {
    title: "Analytics",
    link: "https://otw.kongkowitpku.xyz/stats",
    category: lainnya,
    new: false,
    divider: false,
  },
  {
    title: "Source Code",
    link: "https://otw.kongkowitpku.xyz/view-source",
    category: lainnya,
    new: false,
    divider: false,
  },
];

export default initialDetails;
