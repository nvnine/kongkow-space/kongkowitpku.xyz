import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="id">
      <Head>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="./favicon/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="./favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="./favicon/favicon-16x16.png"
        />
        <link rel="manifest" href="./favicon/site.webmanifest" />
        <link
          rel="mask-icon"
          href="./favicon/safari-pinned-tab.svg"
          color="#292b2b"
        />
        <link rel="shortcut icon" href="./favicon/favicon.ico" />
        <meta name="msapplication-TileColor" content="#ffe5e5" />
        <meta
          name="msapplication-TileImage"
          content="./favicon/mstile-144x144.png"
        />
        <meta
          name="msapplication-config"
          content="./favicon/browserconfig.xml"
        />
        <meta name="theme-color" content="#ffffff" />

        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow" />

        {/* Umami Analytics */}
        <script
          defer
          data-website-id="f25f4aab-f2a1-414e-90ec-cf066e4a287d"
          data-domains="kongkowitpku.xyz"
          src="https://sky.moojok.xyz/cloud.js"
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
