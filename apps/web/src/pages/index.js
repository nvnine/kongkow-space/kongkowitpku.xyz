import anime from "animejs";
import Card from "@/components/card";
import Menu from "@/components/menu";
import Interact from "interactjs";
import { useEffect } from "react";
import splt from "spltjs";

const Home = () => {
  useEffect(() => {
    splt({
      reveal: true,
    });

    anime({
      targets: "h2",
      opacity: [0, 1],
      duration: 2000,
      easing: "cubicBezier(.7,-0.8,.4,1.7)",
    });

    anime({
      targets: ".reveal",
      translateY: [72, 0],
      duration: 1400,
      delay: anime.stagger(30),
      easing: "cubicBezier(.7,-1,.3,1.9)",
    });

    anime({
      targets: "body",
      scale: [1.5, 1],
      duration: 2000,
      easing: "cubicBezier(.82,.05,.33,.96)",
    });

    anime({
      targets: ".social-container",
      opacity: [0, 1],
      delay: 400,
      duration: 1400,
      easing: "cubicBezier(.7,-0.8,.4,1.7)",
    });

    anime({
      targets: ".map",
      translateY: [140, 0],
      delay: 1000,
      duration: 1200,
      easing: "cubicBezier(.9,-0.5,.2,1.5)",
    });

    let scaleDot = 1;
    let scaleSite = 1;

    const windowSize = () => {
      splt.revert();
      if (window.innerWidth < 500) {
        scaleSite = 0.6;
        scaleDot = 2;
      } else {
        scaleSite = 1;
        scaleDot = 1;
      }
    };
    // windowSize();
    window.addEventListener("resize", windowSize);

    const cards = document.querySelectorAll(".card-container");
    let num = 0;
    cards.forEach((card) => {
      num = num + 1;
      let text = num.toString();
      card.classList.add("number_" + text);
      card.style.gridColumnStart = "a" + text;
      card.style.gridColumnEnd = "a" + text;
      card.style.gridRowStart = "a" + text;
      card.style.gridRowEnd = "a" + text;

      if (num % 2 === 0) {
        card.classList.add("even");
      } else {
        card.classList.add("odd");
      }
    });

    const positionDot = document.querySelector(".position");
    const position = { x: 0, y: 0 };

    Interact(".grid").draggable({
      inertia: {
        resistance: 2,
        endSpeed: 1,
        smoothEndDuration: 2000,
      },
      modifiers: [
        Interact.modifiers.restrictRect({
          restriction: "parent",
          endOnly: true,
        }),
      ],
      listeners: {
        move(event) {
          splt.revert();
          position.x += event.dx;
          position.y += event.dy;
          event.target.style.transform = `translate(${position.x}px, ${position.y}px) scale(${scaleSite})`;

          positionDot.style.transform = `translate(${-position.x / 30}px, ${
            -position.y / 30
          }px)  scale(${scaleDot})`;
        },
      },
    });

    Interact(".grid").styleCursor(false);
  }, []);

  return (
    <>
      <div className="App">
        <Menu />
        <div className="grid">
          <div className="title">
            <h2>
              <span className="splt">Kongkow IT Pekanbaru</span> komunitas yang
              membahas segala hal seputar teknologi.
            </h2>
            <div className="social-container">
              <a href="https://otw.kongkowitpku.xyz/instagram" target="_blank">
                <h3>Instagram</h3>
              </a>
              <h3 className="divider"> / </h3>
              <a href="https://otw.kongkowitpku.xyz/telegram" target="_blank">
                <h3>Telegram</h3>
              </a>
              <h3 className="divider"> / </h3>
              <a href="https://otw.kongkowitpku.xyz/discord" target="_blank">
                <h3>Discord</h3>
              </a>
            </div>
          </div>
          <Card
            title="Good Morning"
            video="./videos/morning"
            alt="Latest trending news"
            link="https://morning.kongkow.space"
          />
          <Card
            title="Kongkow Space"
            video="./videos/on"
            alt="Awesome apps, services, products and communities made by Indonesians"
            link="https://kongkow.space"
          />
          <Card
            title="Pakde.AI"
            video="./videos/pakde"
            alt="Stupid ai for stupid things"
            link="https://pakde.moojok.xyz"
          />
          <Card
            title="WC Umum"
            video="./videos/wcumum"
            alt="Pemetaan semua toilet yang dapat diakses publik"
            link="https://wcumum.kongkow.space"
          />
          <Card
            title="Planet"
            video="./videos/planet"
            alt="Super Space"
            link="https://otw.kongkowitpku.xyz/planet"
          />
        </div>
        <div className="map">
          <div className="position"></div>
        </div>
      </div>
    </>
  );
};

export default Home;
