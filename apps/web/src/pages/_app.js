import "@/styles/globals.scss";
import { Analytics } from "@vercel/analytics/react";
import LocalFont from "next/font/local";
import { DefaultSeo } from "next-seo";

const apfel = LocalFont({
  src: [
    {
      path: "../assets/fonts/ApfelGrotezk-Regular.woff2",
      style: "normal",
      weight: "400",
    },
  ],
});

const App = ({ Component, pageProps }) => {
  const title = "Kongkow IT Pekanbaru";
  const description =
    "Komunitas berbasis di Pekanbaru, Indonesia membahas segala hal yang berbau teknologi.";
  const url = "https://kongkowitpku.xyz";

  return (
    <>
      <DefaultSeo
        title={title}
        description={description}
        canonical="https://kongkowitpku.xyz"
        openGraph={{
          url,
          title,
          description,
          type: "website",
          images: [
            {
              url: "https://kongkowitpku.xyz/og.jpg",
              width: 2333,
              height: 1313,
              alt: "Komunitas berbasis di Pekanbaru, Indonesia membahas segala hal yang berbau teknologi.",
              type: "image/jpeg",
            },
          ],
          site_name: "Kongkow IT Pekanbaru",
        }}
      />
      <style jsx global>{`
        :root {
          --apfel: ${apfel.style.fontFamily};
        }
      `}</style>
      <Component {...pageProps} />

      {/* Vercel Analytics */}
      <Analytics />
    </>
  );
};

export default App;
